# DOCKER-VERSION 0.11.1
# LOCAL BUILD ID 12345678-1234-1234-1234-1234567890ab
FROM occam/simulator-707e0416-a372-11e4-9c8a-001fd05bb228-bbedcaae92286a48ac65f3d5e94ec3137d7b825a
ADD . /occam/trace-generator-99f56bec-a4d8-11e4-b31b-001fd05bb228
VOLUME ["/occam/trace-generator-99f56bec-a4d8-11e4-b31b-001fd05bb228"]
CMD cd /occam/trace-generator-99f56bec-a4d8-11e4-b31b-001fd05bb228; /bin/bash -c 'source /occam/setup_python.sh; source /occam/setup_perl.sh; cd /job;  pyenv local 2.7.6; python /occam/trace-generator-99f56bec-a4d8-11e4-b31b-001fd05bb228/launch.py'
